# README #

This project contains a small project with HTML/CSS.  It has three content files:
- index.html
- style.css
- Rosie.jpg



### What is this repository for? ###

* It is intended to be used as a base repository to demonstrate git
* Version 1.0

### How do I get set up? ###

* You will need
1) Git
2) A browser
3) Something that will allow you to edit a plain text file.

* Use of this repository is detailed [here](https://charles_hoot@bitbucket.org/lessons_library_team/lessons.git)



### Who do I talk to? ###

* Denise Case
* Charles Hoot

* * *
This README is based off of the template that Atlassian's bitbucket creates for new projects created using their web interface. [Go to bitbucket](https://bitbucket.org) 
